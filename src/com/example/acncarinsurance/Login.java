package com.example.acncarinsurance;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity {


	DBManager db = new DBManager(this);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		ActionBar actionBar = getActionBar();
		actionBar.hide();

		// Two input boxes - take in user name and password
		final EditText email = (EditText) findViewById(R.id.userName);
		final EditText password = (EditText) findViewById(R.id.password);
		// Login Button
		Button loginBtn = (Button) findViewById(R.id.loginBtn);


		// Checking if button was clicked...
		loginBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				db.open();

				// Checking if the username and password is correct - if so toast message and go to next activity
				if (db.getUser(email.getText().toString(),password.getText().toString()) == true) {

					Toast.makeText(getApplicationContext(),
							"Successful Login", Toast.LENGTH_SHORT).show();

					db.close();

					Intent main = new Intent(getApplicationContext(), Menu.class);
					startActivity(main);

				}
				// otherwise - toast sayin error !
				else {
					Toast.makeText(getApplicationContext(), "No matching username and password found",
							Toast.LENGTH_LONG).show();

				}

			}
		});

		Button RegBtn = (Button) findViewById(R.id.RegBtn);


		// Checking if button was clicked...
		RegBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent main = new Intent(getApplicationContext(), Register.class);
				startActivity(main);

			}
		});

	}

}
