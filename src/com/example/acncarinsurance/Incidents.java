package com.example.acncarinsurance;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Incidents extends Activity {

	Button emailUs, callNow1, callNow2, callNow3;
	
	TextView website;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.incidents_layout);

		ActionBar actionBar = getActionBar();
		actionBar.hide();

		emailUs = (Button) findViewById(R.id.emailUs);
		callNow1 = (Button) findViewById(R.id.callNowI1);
		callNow2 = (Button) findViewById(R.id.callNowI2);
		callNow3 = (Button) findViewById(R.id.callNowI3);
		
		website = (TextView) findViewById(R.id.textView2);

		emailUs.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent email = new Intent(Intent.ACTION_SEND);

				email.putExtra(Intent.EXTRA_EMAIL,
						new String[] { "matlevi10@gmail.com" });
				email.putExtra(Intent.EXTRA_SUBJECT, "Insurance Enquiry");
				email.putExtra(Intent.EXTRA_TEXT, "Message here !");

				email.setType("message/rfc882");
				startActivity(Intent.createChooser(email,
						"Select email client !"));

			}
		});

		callNow1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:0873259173"));
				startActivity(callIntent);

			}
		});

		callNow2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:0873259173"));
				startActivity(callIntent);

			}
		});

		callNow3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:0873259173"));
				startActivity(callIntent);

			}
		});
	
	website.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
					.parse("http://www.google.ie"));
			startActivity(browserIntent);
		}
	});
	
	}

}
