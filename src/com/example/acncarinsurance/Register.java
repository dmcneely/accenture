package com.example.acncarinsurance;


import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Register extends Activity {

	
	DBManager db = new DBManager(this);
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);

		ActionBar actionBar = getActionBar();
		actionBar.hide();
		
		final EditText firstname = (EditText)findViewById(R.id.firstname);
		final EditText surname = (EditText)findViewById(R.id.surname);
		final EditText date = (EditText)findViewById(R.id.date);
		final EditText address = (EditText)findViewById(R.id.address);
		final EditText email = (EditText)findViewById(R.id.email);
		final EditText password = (EditText)findViewById(R.id.password);

		final Button btn = (Button)findViewById(R.id.submitBtn);

		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				db.open(); 				// Open Database
				db.insertUser("" + firstname.getText(), "" +surname.getText(), "" +date.getText(), "" +address.getText(), "" +email.getText(), "" +password.getText());
				db.close();				// Close Database
				finish();
			}
		});
	}
}
