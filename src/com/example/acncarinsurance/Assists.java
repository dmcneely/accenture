package com.example.acncarinsurance;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.content.Context;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

public class Assists extends Activity implements LocationListener{

	Button ourWeb, callNow1, callNow2, map, stations;
	private TextView locationText, acninsure;
	private LocationManager locationManager;
	double  Longitude = 0;
	double Latitude =0;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.assists_layout);

		ActionBar actionBar = getActionBar();
		actionBar.hide();

		locationText = (TextView)findViewById(R.id.textView6);
		setUpLocation();

		ourWeb = (Button) findViewById(R.id.ourWeb);
		callNow1 = (Button) findViewById(R.id.callNow2);
		callNow2 = (Button) findViewById(R.id.callNow3);
		stations = (Button) findViewById(R.id.stations_btn);
		acninsure = (TextView) findViewById(R.id.textView5);

		map = (Button) findViewById(R.id.map_btn);
		map.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.de/maps/search/?q=" + Longitude + "," + Latitude +""));
				startActivity(browserIntent);
			}
		});

		stations.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.de/maps/search/gas+station/?q=" + Longitude + "," + Latitude +""));
				startActivity(browserIntent);
			}
		});
		ourWeb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent smsIntent = new Intent(Intent.ACTION_VIEW);
				smsIntent.setType("vnd.android-dir/mms-sms");
				smsIntent.putExtra("address", "0852060522");
				smsIntent.putExtra("sms_body","I have been involved in an accident");
				startActivity(smsIntent);
			}
		});


		callNow1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:0873259173"));
				startActivity(callIntent);
			}
		});

		callNow2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:0852060522"));
				startActivity(callIntent);

			}
		});

		acninsure.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.acninsurance.ie/"));
				startActivity(browserIntent);

			}
		});
	}
	private void setUpLocation() {
		// TODO Auto-generated method stub
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 5, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onLocationChanged(Location location) {

		String latestLocation ="";

		if(location != null)
		{
			latestLocation = String.format("Current Location \n Longitude: %1$s \n Latitude: %2$s", location.getLongitude(), location.getLatitude());
			Longitude= location.getLongitude();
			Latitude= location.getLatitude();
		}

		Toast.makeText(Assists.this, latestLocation, Toast.LENGTH_LONG).show();
		locationText.setText(latestLocation);
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume()
	{
		super.onResume();
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 5, this);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		locationManager.removeUpdates(this);
	}
}
