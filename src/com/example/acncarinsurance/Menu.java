package com.example.acncarinsurance;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Menu extends Activity implements OnClickListener {

	Button claims, assists, incidents;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_layout);

		ActionBar actionBar = getActionBar();
		actionBar.hide();
		
		claims = (Button) findViewById(R.id.claims);
		assists = (Button) findViewById(R.id.assists);
		incidents = (Button) findViewById(R.id.incidents);

		claims.setOnClickListener(this);
		assists.setOnClickListener(this);
		incidents.setOnClickListener(this);

	}

	public void onClick(View v) {
		// TODO Auto-generated method stub

		// Intents used to go to different classes
		Intent Claims = new Intent(this, Claims.class);
		Intent Assists = new Intent(this, Assists.class);
		Intent Incidents = new Intent(this, Incidents.class);

		// Depending on the button ID being pressed, certain action will be
		// executed.
		switch (v.getId()) {

		case R.id.claims:
			startActivity(Claims);
			// overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

			break;
		case R.id.assists:
			startActivity(Assists);
			// overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

			break;
		case R.id.incidents:
			startActivity(Incidents);
			// overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

			break;
		}

	}

}
