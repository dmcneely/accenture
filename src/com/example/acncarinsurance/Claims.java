package com.example.acncarinsurance;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class Claims extends Activity {

	Spinner spinner1;
	RadioGroup radioGroup;
	RadioButton radioBtn;

	Button submit, Photo;
	EditText claimsDesc;

	String getState;

	ArrayAdapter<String> adapter1;
	String partDamaged[] = { "Windshield", "Bumper", "Door", "Light", "Mirror", "Wheel", "Engine", "Other"};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.claims_layout);

		ActionBar actionBar = getActionBar();
		actionBar.hide();

		spinner1 = (Spinner) findViewById(R.id.spinner2);
		radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		submit = (Button) findViewById(R.id.submit);
		Photo = (Button) findViewById(R.id.getPhoto);

		claimsDesc = (EditText) findViewById(R.id.claimsDesc);

		adapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, partDamaged);

		spinner1.setAdapter(adapter1);

		spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long id) {
				// TODO Auto-generated method stub

				spinner1.setSelection(position);
				getState = (String) spinner1.getSelectedItem();

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		Photo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(Intent.createChooser(intent,
						"Select Picture"),1);
			}
		});
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent email = new Intent(Intent.ACTION_SEND);

				int selectedBtn = radioGroup.getCheckedRadioButtonId();
				radioBtn = (RadioButton) findViewById(selectedBtn);

				String radioCheck = (String) radioBtn.getText();
				Editable check = claimsDesc.getText();



				email.putExtra(Intent.EXTRA_EMAIL, new String[]{"matlevi10@gmail.com"});
				email.putExtra(Intent.EXTRA_SUBJECT, "Minor Claim");
				email.putExtra(Intent.EXTRA_TEXT, "Claim No. 1293 \nParty: " + radioCheck + "\nPart damaged: " + getState
						+ "\nDescription: " + check);

				email.setType("message/rfc882");
				startActivity(Intent.createChooser(email, "Select email client !"));
			}
		});
	}

}
