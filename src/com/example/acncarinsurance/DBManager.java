package com.example.acncarinsurance;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBManager 
{
	public static final String KEY_USERID = "_id";
	public static final String KEY_FNAME = "firstname";
	public static final String KEY_SNAME = "secondname";
	public static final String KEY_ADDRESS = "address";
	public static final String KEY_DOB = "dob";
	public static final String KEY_Email = "email";
	public static final String KEY_PASSWORD = "pass";
	
	public static final String KEY_CLAIMID = "id";
	public static final String KEY_PARTY = "party";
	public static final String KEY_DAMAGED = "partdamaged";
	public static final String KEY_DESC = "description";
	public static final String KEY_PHOTO = "photo";
	
	private static final String DATABASE_NAME = "Database";
	private static final String DATABASE_TABLE1 = "Users";
	private static final String DATABASE_TABLE2 = "Claims";
	private static final int DATABASE_VERSION = 1;
					 
	
	private Context context;
	
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;
	
	/* Database class handles all the queries */
	
	public DBManager(Context ctx)
	{
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper
	{
		DatabaseHelper(Context context)
		{
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) 
		{
			// Creating tables 
			String DATA1_CREATE = "create table " + DATABASE_TABLE1 + "(_id integer primary key autoincrement, " 
			+ "firstname text not null, " 
			+ "secondname text not null," 
			+ "address text not null," 
			+ "dob text not null," 
			+ "email text not null," 
			+ "pass text not null);";
			
			String DATA2_CREATE = "create table " + DATABASE_TABLE2 + "(_id integer primary key autoincrement, "
			+ "id integer not null, "
			+ "party text not null, "
			+ "partdamaged text not null, "
			+ "description text not null, "
			+ "photo text not null); ";
			
			db.execSQL(DATA1_CREATE);
			db.execSQL(DATA2_CREATE);

			//default inserts  
			db.execSQL("INSERT INTO " + DATABASE_TABLE1 + " (firstname, secondname, address, dob, email, pass) Values ('Darren' , 'McNeely' , '13 MapleDrive', '20_05_93', 'email', 'pass')");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
		{
			// drops table if it- exists
			db.execSQL("DROP TABLE IF EXISTS" + DATABASE_TABLE1);

	        // create fresh books table
	        this.onCreate(db);
		}
	}
	
	public DBManager open() throws SQLException
	{
		db = DBHelper.getWritableDatabase();
		return this;
	}
	
	public void close()
	{
		DBHelper.close();
	}
	
	public long insertUser(String firstname, String secondname, String address, String dob, String email, String pass)
	{
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_FNAME, firstname);
		initialValues.put(KEY_SNAME, secondname);
		initialValues.put(KEY_ADDRESS, address);
		initialValues.put(KEY_DOB, dob);
		initialValues.put(KEY_Email, email);
		initialValues.put(KEY_PASSWORD, pass);
		return db.insert(DATABASE_TABLE1, null, initialValues);
	}
	
		
	public Cursor getAllUsers()
	{
		return db.query(DATABASE_TABLE1, new String[] {
						KEY_USERID,
						KEY_FNAME,
						KEY_SNAME,
						KEY_ADDRESS,
						KEY_DOB, 
						KEY_Email,
						KEY_PASSWORD},
						null,
						null,
						null,
						null,
						null);
	}
	
	public Cursor getUser(String rowId) throws SQLException
	{
		Cursor mCursor =
				db.query(true, DATABASE_TABLE1, new String[] {
						KEY_USERID,
						KEY_FNAME,
						KEY_SNAME,
						KEY_ADDRESS,
						KEY_DOB, 
						KEY_Email,
						KEY_PASSWORD
						},
						KEY_USERID + "='" + rowId + "'",
						null,
						null,
						null,
						null,
						null);
		
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}
	
	public boolean getUser(String email, String pass) throws SQLException
	{
		Cursor mCursor =
				db.query(true, DATABASE_TABLE1, new String[] {
						KEY_USERID,
						KEY_FNAME,
						KEY_SNAME,
						KEY_ADDRESS,
						KEY_DOB, 
						KEY_Email,
						KEY_PASSWORD
						},
						KEY_Email + "='" + email + "'" + " and " + KEY_PASSWORD + "='" + pass + "'" ,
						null,
						null,
						null,
						null,
						null);
		
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		
		if(mCursor.getCount() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}